<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
    version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    
    <xsl:output 
        method="text" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="no"/>

    <xsl:template match="IDG"/>
    
    <xsl:template match="PB">
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>TCP_PB:</xsl:text><xsl:value-of select="@REF"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="P|L|LG|DIV1|HEAD|LIST|ITEM|CLOSER|TRAILER|OPENER|NOTE|FIGURE|FIGDESC|CELL|BIBL|ROW">
        <xsl:text>&#x0a;</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>&#x0a;</xsl:text>
    </xsl:template>

    <xsl:template match="NOTE[@N]">
        <xsl:text> </xsl:text>
        <xsl:value-of select="@N"/>
        <xsl:text> </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="GAP[@DESC = 'duplicate' or @DESC = 'symbol']">
        <xsl:text>  </xsl:text>
    </xsl:template>

    <xsl:template match="GAP[not(@DESC = 'duplicate') and not(@DESC = 'symbol')]">
        <xsl:choose>
            <xsl:when test="@DISP = '〈 in non-Latin alphabet 〉'">〈◊〉 〈◊〉 〈◊〉 〈◊〉 〈◊〉</xsl:when>
            <xsl:when test="@DISP = '〈◊◊〉'">〈◊〉 〈◊〉</xsl:when>
            <xsl:when test="@DISP = '〈◊◊◊〉'">〈◊〉 〈◊〉 〈◊〉</xsl:when>
            <xsl:when test="@DISP = '〈◊◊◊◊〉'">〈◊〉 〈◊〉 〈◊〉 〈◊〉</xsl:when>
            <xsl:when test="contains(@EXTENT, 'page')">〈◊〉</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="replace(@DISP, '•', '●')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="SUP">
        <xsl:choose>
            <xsl:when test=". = 'a'">ᵃ</xsl:when>
            <xsl:when test=". = 'b'">ᵇ</xsl:when>
            <xsl:when test=". = 'c'">ᶜ</xsl:when>
            <xsl:when test=". = 'd'">ᵈ</xsl:when>
            <xsl:when test=". = 'e'">ᵉ</xsl:when>
            <xsl:when test=". = 'f'">ᶠ</xsl:when>
            <xsl:when test=". = 'g'">ᵍ</xsl:when>
            <xsl:when test=". = 'h'">ʰ</xsl:when>
            <xsl:when test=". = 'i'">ⁱ</xsl:when>
            <xsl:when test=". = 'j'">ʲ</xsl:when>
            <xsl:when test=". = 'k'">ᵏ</xsl:when>
            <xsl:when test=". = 'l'">ˡ</xsl:when>
            <xsl:when test=". = 'm'">ᵐ</xsl:when>
            <xsl:when test=". = 'n'">ⁿ</xsl:when>
            <xsl:when test=". = 'o'">ᵒ</xsl:when>
            <xsl:when test=". = 'p'">ᵖ</xsl:when>
            <xsl:when test=". = 'r'">ʳ</xsl:when>
            <xsl:when test=". = 's'">ˢ</xsl:when>
            <xsl:when test=". = 't'">ᵗ</xsl:when>
            <xsl:when test=". = 'u'">ᵘ</xsl:when>
            <xsl:when test=". = 'v'">ᵛ</xsl:when>
            <xsl:when test=". = 'w'">ʷ</xsl:when>
            <xsl:when test=". = 'x'">ˣ</xsl:when>
            <xsl:when test=". = 'y'">ʸ</xsl:when>
            <xsl:when test=". = 'z'">ᶻ</xsl:when>
            <xsl:when test=". = '1'">¹</xsl:when>
            <xsl:when test=". = '2'">²</xsl:when>
            <xsl:when test=". = '3'">³</xsl:when>
            <xsl:when test=". = '4'">⁴</xsl:when>
            <xsl:when test=". = '5'">⁵</xsl:when>
            <xsl:when test=". = '6'">⁶</xsl:when>
            <xsl:when test=". = '7'">⁷</xsl:when>
            <xsl:when test=". = '8'">⁸</xsl:when>
            <xsl:when test=". = '9'">⁹</xsl:when>
            <xsl:when test=". = '0'">⁰</xsl:when>
            <xsl:when test=". = 'ii'">ⁱⁱ</xsl:when>
            <xsl:when test=". = 'xx'">ˣˣ</xsl:when>
            <xsl:when test=". = 'am'">ᵃᵐ</xsl:when>
            <xsl:when test=". = 'th'">ᵗʰ</xsl:when>
            <xsl:when test=". = 'ch'">ᶜʰ</xsl:when>
            <xsl:when test=". = 'A'">ᴬ</xsl:when>
            <xsl:when test=". = 'B'">ᴮ</xsl:when>
            <xsl:when test=". = 'D'">ᴰ</xsl:when>
            <xsl:when test=". = 'E'">ᴱ</xsl:when>
            <xsl:when test=". = 'G'">ᴳ</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="text()">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="/">
        <xsl:apply-templates select="/ETS/EEBO"/>
    </xsl:template>

</xsl:stylesheet>
