#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, re, subprocess

#
#   INPUT FOLDERS; THESE WILL CHANGE DEPENDING ON WHERE THE SCRIPT RUNS.
#
#   RUN LIKE:
#
#   nohup python -u which_7000.py 
#

if __name__ == '__main__':

    OLD_FOLDER = '/home/spenteco/0/eebotcp.OLD/texts/*/*.xml'
    NEW_FOLDER = '/home/spenteco/0/eebotcp/texts/*/*.xml'
    
    old_files = set(list([re.sub('.+\/texts\/', '', f) for f in glob.glob(OLD_FOLDER)]))
    new_files = set(list([re.sub('.+\/texts\/', '', f) for f in glob.glob(NEW_FOLDER)]))
    
    for f in old_files:
        if f in new_files:
            cmd = 'diff /home/spenteco/0/eebotcp.OLD/texts/' + f + \
                        ' ' + \
                        '/home/spenteco/0/eebotcp/texts/' + f
            print('cmd', cmd)
            print('output', subprocess.getoutput(cmd))
        else:
            print('in old, not in new', f)
    
    for f in new_files:
        if f in old_files:
            pass
        else:
            print('in new, not in old', f)
