#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, json, csv, sys
from lxml import etree
from queue import Queue
from threading import Thread

#
#   INPUT FOLDERS; THESE WILL CHANGE DEPENDING ON WHERE THE SCRIPT RUNS.
#
#   RUN LIKE:
#
#   nohup python -u add_am_orig_text.py LOCATIONS_classified_results_eebochron/ '/home/spenteco/0/eebotcp/texts/*/*.xml' LOCATIONS_final_results_eebochron/ 4 > OUT.add_am_orig_text.txt 2>&1 &
#

def add_em(q, i, OUTPUT_FOLDER):
    
    while True:
        
        data = q.get()
            
        p = data[0]
        path_to_xml = data[1]
            
        print('processing', p)
        
        tcp_id = p.split('/')[-1].split('.')[0]
        
        diff_results = json.load(open(p, 'r', encoding='utf-8'))
        
        tree = etree.parse(path_to_xml)
        
        for d in diff_results[1]:
                    
            am_orig = []
            am_text = []
            
            for am_id in d['am_ids']:
            
                try:
            
                    for w in tree.xpath('//tei:w[@xml:id="' + am_id + '"]', 
                                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                                            
                        if w.get('orig') != None:
                            am_orig.append(w.get('orig'))
                        else:
                            am_orig.append('')
                            
                        if w.text != None:
                            am_text.append(w.text)
                        else:
                            am_text.append('')
                except etree.XPathEvalError:
                    pass
            
            d['am_orig'] = am_orig      
            d['am_text'] = am_text 
            
        f = open(OUTPUT_FOLDER + tcp_id + '.js', 'w', encoding='utf-8')
        f.write(json.dumps(diff_results))
        f.close()  
            
        q.task_done()
    
# ======================================================================

if __name__ == '__main__':

    DIFF_RESULTS_FOLDER = sys.argv[1]
    GLOB_PATTERN = sys.argv[2]
    OUTPUT_FOLDER = sys.argv[3]
    NUM_THREADS = int(sys.argv[4])

    ids_to_paths = {}
    for p in glob.glob(GLOB_PATTERN, recursive=True):
        k = p.split('/')[-1].split('.')[0].split('-')[-1]
        print('p', p, 'k', k)
        ids_to_paths[k] = p
        
    print('len(ids_to_paths)', len(ids_to_paths))
    
    processed_ids = [p.split('/')[-1].split('.')[0].split('-')[-1] for p in glob.glob(OUTPUT_FOLDER + '*.js')]
        
    processed_ids = set(processed_ids)
        
    print('len(processed_ids)', len(processed_ids))
    
    q = Queue(maxsize=0)

    for n, p in enumerate(glob.glob(DIFF_RESULTS_FOLDER + '*.js')):
    
        tcp_id = p.split('/')[-1].split('.')[0].split('-')[-1]
        
        if tcp_id in processed_ids:
            continue
        
        print('queuing', p, tcp_id)
        
        q.put([p, ids_to_paths[tcp_id]])
        
    print('NUM_THREADS', NUM_THREADS)

    for i in range(NUM_THREADS):
        
        print('starting worker', i)
        
        worker = Thread(target=add_em, args=(q, i, OUTPUT_FOLDER))
        worker.setDaemon(True)
        worker.start()
        
    q.join()

    print('Done!')
    
