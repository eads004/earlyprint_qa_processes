# Earyprint QA Processes #

This repo contains set of processes for comparing EEBO-TCP XML to the Earlyprint XML corpus.

**SEE BELOW FOR "7,000" PROCESSES**

### Create results folders ###

The process is going to write three files for every file in the Earlyprint XML corpus, and they need to go somewhere.  So:

```
mkdir LOCATIONS_results_eebochron
mkdir LOCATIONS_classified_results_eebochron
mkdir LOCATIONS_am_orig_and_text
mkdir long_tail_tsv
```

### Find the differences ###

First, we find the differences between the corpora.  The command-line arguments for the script ("TCP_LOCATIONS_multithread_check_em_all.py") are:

```
TCP_LOCATIONS_multithread_check_em_all.py 
    path_to_eebo_tcp_folder
    path_to_earlyprint_folder 
    results_folder 
    n_threads
    tcp_id_to_process [optional argument]
```

The script depends on lxml (a python package installed with pip) and saxon (the path to the saxon jar is in line 8 of the script; you will need to change this line to point to saxon on your computer).  It uses a couple of stylesheets (extract_am_text_and_ids.xsl and extract_tcp_plaintext_and_locations.xsl) to extract plain text from the XML.  It also uses the diff command native to bash.

The script spreads its work across a number of threads, as specified on the command line.  This multithreading makes it possible to compare the full Earlyprint corpus to EEBO-TCP in a reasonable amount of time.  Note that each thread writes three or four temp files; however, these files are deleted at the end of the process (see line 493 of the script).  It writes one difference file (a json file) for every Earlyprint XML file.

If you're test-driving the process using the test XML included in this repo, and you want to process all the files, then the command is like:

```
nohup python -u TCP_LOCATIONS_multithread_check_em_all.py test_eebo_tcp_xml/  'test_earlyprint_xml/**/' LOCATIONS_results_eebochron/ 3 > OUT.check.txt 2>&1 &
```

If you'd like to run the process on just one TCP ID (A00277 in this example), then the command is like:

```
nohup python -u TCP_LOCATIONS_multithread_check_em_all.py test_eebo_tcp_xml/  'test_earlyprint_xml/**/' LOCATIONS_results_eebochron/ 3 A00277 > OUT.check.txt 2>&1 &
```

* "nohup" tells the shell to detach the command from the shell; 
* "python -u" means that python will not buffer stdout; 
* " > OUT.check.txt 2>&1" means, "send all stdout and stderr--i.e., all the messages that would normally get sent to the terminal--to a file called OUT.check.txt; 
* the final "&" tells the shell to run the command in the background.  

Note the "\*\*" in "test_earlyprint_xml/\*\*/"; it's necessary when specifying a path the earlyprint folder.

When I run the command on my workstation on all the data, I run something like:

```
nohup python -u TCP_LOCATIONS_multithread_check_em_all.py /home/spenteco/0/eebo_012617/ '/home/spenteco/0/eebochron_dropbox/**/' LOCATIONS_results_eebochron/ 6 > OUT.check.txt 2>&1 &
```

or like:

```
nohup python -u TCP_LOCATIONS_multithread_check_em_all.py /home/spenteco/0/eebo_012617/ '/home/spenteco/0/eebotcp/**/' LOCATIONS_results_eebochron/ 6  new_tcp_ids.txt > OUT.check.txt 2>&1 &
```


### Classify the results ###

Notebook **01_examine_ALL_results.ipynb** classifies differences according to their type.

It reads the results from TCP_LOCATIONS_multithread_check_em_all.py, and writes a new set of results to  LOCATIONS_classified_results_eebochron.  It also writes defects_LOCATIONS_results_eebochron_020221.csv for reasons which escape me.


### Recover the locations of differences ###

For the life of me, I cannot rememeber what this script does.  But it has to be run; otherwise, the notebook for counting exceptions by type won't work.

The command-line parameters for the script ("TCP_LOCATIONS_multithread_check_em_all") are:

```
add_am_orig_text.py 
    path_to_the_results_from_TCP_LOCATIONS_multithread_check_em_all.pt
    path_to_earlyprint_folder 
    results_folder 
    n_threads
```

If you're test-driving the process using the test XML included in this repo, then the command is like:

```
python -u add_am_orig_text.py LOCATIONS_classified_results_eebochron/ 'test_earlyprint_xml/**/*.xml' LOCATIONS_am_orig_and_text/ 3  > OUT.add_am_orig_text.txt 2>&1 &
```

When I run the command on my workstation on all the data, I run something like:

```
python -u add_am_orig_text.py LOCATIONS_classified_results_eebochron/ '/home/spenteco/0/eebochron_dropbox/**/*.xml' LOCATIONS_am_orig_and_text/ 6  > OUT.add_am_orig_text.txt 2>&1 &
```

### Prepare the spreadsheets of error types and counts ###

Notebook **03_extract_spreadsheets.ipynb** reads the contents of add_am_orig_text.py, and writes out a series of csv files, one per type of error, to long_tail_tsv.


### Find examples of certain types of problems ###

Occasionally, it's convenient to pull examples of various kinds or error.  **02_find_certain_problems.ipynb** helps with this.


# 7,000" PROCESSES #

Martin finished the last 7,000.
