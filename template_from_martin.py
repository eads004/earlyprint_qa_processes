#!/usr/bin/python
# -*- coding: utf-8 -*-


import re
from lxml import etree
import os
from collections import defaultdict
from time import process_time





#lxml variables
xml = '{http://www.w3.org/XML/1998/namespace}'
tei = '{http://www.tei-c.org/ns/1.0}'
parser = etree.XMLParser(remove_blank_text=True, collect_ids=False) #strips outside whitespace and hash  for ids

punctuation = (',', '.', ';', ':', '?' )

single_capital = re.compile(r'^[A-Z][a-z]?$')
single_lowercase = re.compile(r'^[a-z]$')
lowercase = re.compile(r'[a-z]+')
tcp = re.compile(r'[AB]\d{5}')
digit = re.compile(r'[0-9]')

# directories
dropbox = os.path.join('/', 'users', 'martinmueller', 'Dropbox')
earlyprint= os.path.join(dropbox, 'earlyprint')
eebochron = os.path.join(earlyprint, 'eebochron')
eebochrontemp = os.path.join(earlyprint, 'eebochrontemp')
allerlei = os.path.join(dropbox, 'allerlei')

fileout = open(os.path.join(allerlei, 'notes.txt'),'a')

names = defaultdict(int)

# create an etree and loop through each token
def do_etree(filename, item, counter):
    tree = etree.parse(filename,parser )
    for w in tree.iter(tei + 'w'):
        if w.get('reg') is not None and w.get('reg') == w.text:
            w.attrib.pop('reg')
        
    newname = os.path.join(eebochrontempt, 'item')
    sort_and_indent(tree.getroot())
    tree.write(neewname, encoding=tree.docinfo.encoding,
               xml_declaration=True, pretty_print=True)
               
    # add checkem_all.py here??
    
    import subprocess

    cmd = 'cd path_to_check_em_all; python -u TCP_LOCATIONS_multithread_check_em_all.py path_to_tcp/  \'path_to_new_ep_file_folder/**/\' path_to_results_folder/ 1 tcp_id'

    print(subprocess.getoutput(cmd))

# select files to be processed
currentfiles = os.walk(eebochron)
print(eebochron)
counter = 0
for directory in currentfiles :


    for item in sorted(directory[2]):
       if item == '147-bcd-A05232':
           counter += 1
           filename = os.path.join(directory[0], item)
           if counter % 100 == 0:
               do_etree(counter, filename, item)


          # could there be a command here that starts chheck_em_all from scratch on the file requested










