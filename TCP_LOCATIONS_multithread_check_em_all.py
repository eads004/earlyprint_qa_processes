#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, json, subprocess, sys, re, time
from queue import Queue
from threading import Thread
from lxml import etree

PATH_TO_SAXON = '/home/spenteco/0/saxon9he.jar'

def check_em_all(q, i, TCP_FOLDER, AM_FOLDER, RESULTS_FOLDER):
    
    while True:
        
        # =========================================================
        # GET INPUT FILE NAME, TCP_ID
        # =========================================================
        
        start_time = time.time()
        
        am_p = q.get()
        
        k = am_p.split('/')[-1].split('.')[0].split('-')[-1]
        martin_file_name = am_p.split('/')[-1].split('.')[0]
        
        #print('martin_file_name', martin_file_name)
        
        # =========================================================
        # TRANSFORM TCP TO ONE TOKEN PER LINE
        # =========================================================

        #
        # EXTRACT PLAIN TEXT
        #

        cmd = 'java -jar ' + PATH_TO_SAXON + ' ' + \
                TCP_FOLDER + k + '.headed.xml' + \
                ' extract_tcp_plaintext_and_locations.xsl > ' + \
                'temp1.' + str(i) + '.tcp.txt'
                
        #print(cmd)
            
        result = subprocess.getoutput(cmd)

        #
        # FIX THE CURLEY BRACES, ETC
        #

        f = open('temp1.' + str(i) + '.tcp.txt', 'r', encoding='utf-8')
        tcp_text = f.read()
        f.close()

        for l in '∣¦':                                                      #  HYPHENATION
            tcp_text = tcp_text.replace(l, '')
            
        tcp_text = tcp_text.replace('Ʋ', 'V')                               #  CHAR NORM
        
        tcp_text = tcp_text.replace('•', '●') 
        
        curley_replaces = [['{punctel}', ';'], ['{reversed ;}', '⁏'], 
            ['{reversed ¶}', '⁋'], ['{inverted ⁂}', '* ⁎ *'], ['{☞}', '☞'], ['{air}', '🜁'], 
            ['{con}', 'ꝯ'], ['{earth}', '🜃'], ['{fire}', '🜂'], ['{fleur-de-lys}', '⚜'], 
            ['{is}', 'ꝭ'], ['{ou}', 'ȣ'], ['{per}', 'ꝑ'], ['{pre}', 'p̄'], ['{pro}', 'ꝓ'], 
            ['{quod}', 'ꝙ'], ['{QUOD}', 'Ꝙ'], ['{subli}', '🝞'], ['{that}', 'ꝧ'], ['{us}', 'ꝰ'], 
            ['{w}', 'ỽ'], ['{water}', '🜄'], ['{arsenic}', '🜺'], ['{night}', '🝯'], 
            ['{sal armoniac}', '🜹'], ['{vitriol}', '🜖'], ['{salt}', '🜔'], ['{nitre}', '🜕'], 
            ['{alum}', ''], ['{precipi}', '🝟'], ['{tartar}', '🜿'], ['{sulphur}', '🜍'], 
            ['{retort}', '🝭'], ['{vinegar}', '🝭'], ['{vinegar distilled}', '🝭'], 
            ['{quicklime}', '🝁'], ['{orpament}', '🜽'], ['{aqua fortis}', '🜅'], 
            ['{aqua regis}', '🜆'], ['{ashes}', '🝗'], ['{alembic}', '🝪'], ['{cinnabar}', ''], 
            ['{wax}', '🝊'], ['{potash}', '🝘'], ['{oil}', '🝆'], ['{purify}', '🝣'], 
            ['{tartar}', '🜿'], ['{·}', '⚀'], ['{˙.}', '⚁'], ['{::}', '⚃'], ['{:::}', '⚅'], 
            ['{:·:}', '⚄'], ['{˙·.}', '⚂'], ['{f-clef}', '𝄢'], ['{c-clef}', '𝄡'], 
            ['{g-clef}', '𝄞'], ['{F-clef}', '𝄢'], ['{C-clef}', '𝄡'], ['{G-clef}', '𝄞'], 
            ['{powerof1}', '¹'], ['{powerof2}', '²'], ['{powerof3}', '³'], ['{powerof4}', '⁴'], 
            ['{powerof5}', '⁵'], ['{powerof6}', '⁶'], ['{roman 50000}', 'ↇ'], 
            ['{roman 100000}', 'ↈ'], ['{bus}', 'bus'], ['{er}', 'er'], ['{es}', 'es'], 
            ['{etc}', 'etc'], ['{pri}', 'pri'], ['{qu}', 'qu'], ['{quam}', 'quam'], 
            ['{que}', 'que'], ['{qui}', 'qui'], ['{rum}', 'rum'], ['{sed}', 'sed'], 
            ['{ser}', 'ser'], ['{ur}', 'ur'], ['{QUE}', 'QUE'], ['{US}', 'US']]
            
        for curley_replace in curley_replaces:
            tcp_text = tcp_text.replace(curley_replace[0], curley_replace[1])   

        tcp_tokens = []
        tcp_locations = []
        
        last_page_number = ''
        last_page_suffix = ''
        token_number = 0

        for t in re.split('\s+', tcp_text):
            if t > '':
                if t.startswith('TCP_PB:'):
                    
                    token_number = 0
                    
                    next_page_number = t.replace('TCP_PB:', '')
                    
                    if last_page_number == next_page_number:
                        last_page_suffix = 'b'
                    else:
                        last_page_number = next_page_number
                        last_page_suffix = 'a'
                        
                else:
                    tcp_tokens.append(t)
                    tcp_locations.append(last_page_number + '-' + \
                                            last_page_suffix + '-' + \
                                            str(token_number))
                    token_number += 1
            else:
                pass

        f = open('temp2.' + str(i) + '.tcp.txt', 'w', encoding='utf-8')
        f.write('\n' + '\n'.join(tcp_tokens) + '\n\n')
        f.close()


        # =========================================================
        # TRANSFORM AM TO ONE TOKEN PER LINE
        # =========================================================

        #
        # RESOLVE THE @ORIG, EXTRACT TEXT AND @xml:id FROM <w>, <pc> AND <sic>
        #

        cmd = 'java -jar ' + PATH_TO_SAXON + ' ' + \
                am_p + \
                ' extract_am_text_and_ids.xsl > ' + \
                'temp1.' + str(i) + '.am.txt'
            
        result = subprocess.getoutput(cmd)

        #
        # SPLIT TEXT AND ID'S.
        #
        
        f = open('temp1.' + str(i) + '.am.txt', 'r', encoding='utf-8')
        text = f.read()
        f.close() 
        
        am_tokens = []
        am_token_ids = []
        
        for line in text.split('\n'):
            if line.strip() > '':
                
                line_parts = [t for t in re.split('\s+', line.strip()) if t > '']
                
                if len(line_parts) == 2:
                    
                    am_tokens.append(line_parts[0])
                    am_token_ids.append(line_parts[1])
                    
                elif len(line_parts) > 1:
                    for p in line_parts[:-1]:
                        
                        am_tokens.append(p)
                        am_token_ids.append(line_parts[-1])
                            
        #
        # ARGUE THE AM TOKENS INTO A TOKENIZATION SCHEME CONSISTENT WITH
        # WHAT WE DID FOR THE TCP, ABOVE.
        #
        # HANG ON TO final_am_ids FOR USE IN DIFFING. 
        #
        
        final_am_tokens = [am_tokens[0]]
        final_am_ids = [am_token_ids[0]]
        
        for a in range(1, len(am_tokens)):
            
            if am_tokens[a][0] in '!),.:;?}]':
                final_am_tokens[-1] = final_am_tokens[-1] + am_tokens[a]
                final_am_ids[-1] = final_am_ids[-1] + ' ' + am_token_ids[a]
                
            elif final_am_tokens[-1][-1] in '{[(':
                final_am_tokens[-1] = final_am_tokens[-1] + am_tokens[a]
                final_am_ids[-1] = final_am_ids[-1] + ' ' + am_token_ids[a]
                
            else:
                final_am_tokens.append(am_tokens[a])
                final_am_ids.append(am_token_ids[a])

        f = open('temp2.' + str(i) + '.am.txt', 'w', encoding='utf-8')
        f.write('\n' + '\n'.join(final_am_tokens) + '\n')
        f.close()
        
        # =========================================================
        # COMMAND LINE DIFF AND COLLECT RESULTS
        # =========================================================

        #
        # BASH DIFF.  GETTING HERE IS THE POINT OF EVERYTHING ABOVE
        #
            
        cmd = 'diff -y -d temp2.' + str(i) + '.tcp.txt temp2.' + str(i) + '.am.txt > temp.' + str(i) + '.diff.txt'
            
        subprocess.getoutput(cmd)
        
        result = open('temp.' + str(i) + '.diff.txt', 'r', encoding='utf-8').read()

        #
        # LOAD THE DIFF RESULTS
        #
        
        result = open('temp.' + str(i) + '.diff.txt', 'r', encoding='utf-8').read()

        line_parts = []

        for line in result.split('\n'):
            
            if line.strip() > '':
                
                line_parts.append([t for t in re.split(r'(\s+)', 
                                        re.sub(r'\s+', ' ', line))
                                if t > ''])
                  
        #print()
        #print('len(line_parts)', len(line_parts))
        
        tcp_ids_matching_line_results = []
        tcp_token_id_index = -1
        
        am_ids_matching_line_results = []
        am_token_id_index = -1
        
        #print('len(tcp_locations)', len(tcp_locations),
        #        'len(final_am_ids)', len(final_am_ids))
        #print()
        
        for line_part in line_parts:
            
            #print('line_part', line_part)
            
            if line_part[-1] == '<':
                
                tcp_token_id_index += 1
                if tcp_token_id_index < len(tcp_locations):
                    tcp_ids_matching_line_results.append(tcp_locations[tcp_token_id_index])
                else:
                    tcp_ids_matching_line_results.append('')
                    
                am_ids_matching_line_results.append('')
                
            elif line_part[1] == '>':
                
                tcp_ids_matching_line_results.append('')
                
                am_token_id_index += 1
                if am_token_id_index < len(final_am_ids):
                    am_ids_matching_line_results.append(final_am_ids[am_token_id_index])
                else:
                    am_ids_matching_line_results.append('')
            else:
                
                tcp_token_id_index += 1
                if tcp_token_id_index < len(tcp_locations):
                    tcp_ids_matching_line_results.append(tcp_locations[tcp_token_id_index])
                else:
                    tcp_ids_matching_line_results.append('')
                
                am_token_id_index += 1
                if am_token_id_index < len(final_am_ids):
                    am_ids_matching_line_results.append(final_am_ids[am_token_id_index])
                else:
                    am_ids_matching_line_results.append('')
        
        #print()
        #print('len(tcp_ids_matching_line_results)', len(tcp_ids_matching_line_results),
        #        'len(am_ids_matching_line_results)', len(am_ids_matching_line_results))
                
        #
        # INNER FUNCTION CALLED BELOW
        #
                                
        def previous_line_was_difference(diff_line):
            
            if len(diff_line) == 3 and diff_line[2] == '<':
                return True
            elif len(diff_line) == 4 or len(diff_line) == 5:
                return True
            else:
                return False
                
        #
        # LOOK THROUGH THE DIFF RESULTS, PULLING OUT & GROUPING DIFFERENCES
        #

        groups = []

        for a, line in enumerate(line_parts):
            
            if len(line) == 3:
                if line[2] == '<':
                    if a == 0 or previous_line_was_difference(line_parts[a - 1]) == False:
                        
                        groups.append({'tcp': [], 'am': [], 'a': [], 
                                        'am_ids': [],
                                        't_locatons': [],
                                        'am_before': [], 'am_after': [], 
                                        'am_ids_before': [], 'am_ids_after': [],
                                        'tcp_locations': [],
                                        'tcp_before': [], 'tcp_after': [], 
                                        'tcp_locations_before': [], 'tcp_locations_after': []})
                    
                    groups[-1]['tcp'].append(line[0])
                    groups[-1]['a'].append(a)
                    
            elif len(line) == 4:
                if a == 0 or previous_line_was_difference(line_parts[a - 1]) == False:
                        
                    groups.append({'tcp': [], 'am': [], 'a': [],
                                    'am_ids': [],
                                    'am_before': [], 'am_after': [], 
                                    'am_ids_before': [], 'am_ids_after': [],
                                    'tcp_locations': [],
                                    'tcp_before': [], 'tcp_after': [], 
                                    'tcp_locations_before': [], 'tcp_locations_after': []})
                
                groups[-1]['am'].append(line[3])
                groups[-1]['a'].append(a)
            
            elif len(line) == 5:
                if a == 0 or previous_line_was_difference(line_parts[a - 1]) == False:
                        
                    groups.append({'tcp': [], 'am': [], 'a': [], 
                                    'am_ids': [],
                                    'am_before': [], 'am_after': [], 
                                    'am_ids_before': [], 'am_ids_after': [],
                                    'tcp_locations': [],
                                    'tcp_before': [], 'tcp_after': [], 
                                    'tcp_locations_before': [], 'tcp_locations_after': []})
                
                groups[-1]['tcp'].append(line[0])
                groups[-1]['am'].append(line[4])
                groups[-1]['a'].append(a)
                
        final_groups = []
                
        #
        # SOME CULLING, ETC . . . SOME DIFFS ARE NOT REAL DIFFS
        #
                
        for group in groups:
            
            tcp = group['tcp']
            am = group['am']
                    
            gap_tcp = ''.join(tcp)
            gap_am = ''.join(am).replace('〈…〉', '').replace('〈◊〉', '')
                    
            if len(tcp) == 1 and tcp[0] == ''.join(am):                 # TOKENIZATION
                pass
            elif len(am) == 1 and am[0] == ''.join(tcp):                # TOKENIZATION
                pass
            elif len(tcp) == 0 and len(am) == 1 and len(am[0]) == 1:    #  NOTE @N  
                pass
            elif len(tcp) == 1 and len(am) == 1 and \
                    tcp[0].replace('-', '') == am[0].replace('-', ''):  #  HYPENATION
                pass
            elif ''.join(tcp) == ''.join(am):                           #  TOKENIZATION
                pass
            elif gap_tcp == gap_am:                                     #  GAP
                pass
            else:
                final_groups.append(group)
                
        #
        # ADD CONTEXT
        #
                
        #print('len(line_parts)', len(line_parts), 
        #        'len(tcp_ids_matching_line_results)', len(tcp_ids_matching_line_results), 
        #        'len(am_ids_matching_line_results)', len(am_ids_matching_line_results))
        #print()
        
        for group in final_groups:
            
            #print('group[\'a\']', group['a'])
            #print()
            
            a = group['a'][0]
            while len(group['am_before']) < 5:
                a = a - 1
                if a < 0:
                    break
                if line_parts[a][0] > '':
                    group['tcp_before'].append(line_parts[a][0])
                    group['tcp_locations_before'].append(tcp_ids_matching_line_results[a])
                if line_parts[a][-1] > '':
                    group['am_before'].append(line_parts[a][-1])
                    group['am_ids_before'].append(am_ids_matching_line_results[a])
                    
            group['tcp_before'].reverse()
            group['tcp_locations_before'].reverse()
                    
            group['am_before'].reverse()
            group['am_ids_before'].reverse()
                    
            to_context = []
            a = group['a'][-1]
            while len(group['am_after']) < 5:
                a = a + 1
                if a > len(line_parts) -1 :
                    break
                if line_parts[a][-1] > '':
                    group['tcp_after'].append(line_parts[a][0])
                    group['tcp_locations_after'].append(tcp_ids_matching_line_results[a])
                    group['am_after'].append(line_parts[a][-1])
                    group['am_ids_after'].append(am_ids_matching_line_results[a])
                    
            for a in group['a']:
                if tcp_ids_matching_line_results[a] > '':
                    group['tcp_locations'].append(tcp_ids_matching_line_results[a])
                if am_ids_matching_line_results[a] > '':
                    group['am_ids'].append(am_ids_matching_line_results[a])
                
        #
        # WRITE THE RESULT
        #
      
        output_f = open(RESULTS_FOLDER + martin_file_name + '.js', 'w', encoding='utf-8')
        
        output_f.write(json.dumps([martin_file_name, final_groups], indent=4) + '\n\n')
        
        output_f.close()
        
        # =========================================================
        # LIKE A DICKENS NOVEL, THIS COMES TO AN END AFTER FAR TOO LONG.
        # =========================================================
        
        q.task_done()
        
        stop_time = time.time()

        print('done', i, am_p, k, (stop_time - start_time))
    

if __name__ == '__main__':

    TCP_FOLDER = sys.argv[1]
    AM_FOLDER = sys.argv[2]
    RESULTS_FOLDER = sys.argv[3]
    NUM_THREADS = int(sys.argv[4])

    LIMIT_TO_IDS = []
    
    if len(sys.argv) > 5:
        LIMIT_TO_IDS = [l.strip() for l in open(sys.argv[5]).read().split('\n') if l.strip() > '']
        
    print('TCP_FOLDER', TCP_FOLDER)
    print('AM_FOLDER', AM_FOLDER)
    print('RESULTS_FOLDER', RESULTS_FOLDER)
    print('NUM_THREADS', NUM_THREADS)
    print('len(LIMIT_TO_IDS)', len(LIMIT_TO_IDS))
    
    q = Queue(maxsize=0)

    am_paths = [p for p in glob.glob(AM_FOLDER + '*.xml', recursive=True)
                if '_' not in p.split('/')[-1].split('.')[0]]

    print('len(am_paths)', len(am_paths))
    
    processed_ids = [p.split('/')[-1].split('.')[0] for p in glob.glob(RESULTS_FOLDER + '*.js')]

    print('len(processed_ids)', len(processed_ids))
    
    processed_ids = set(processed_ids)
    
    for p in am_paths:
        
        tcp_id = p.split('/')[-1].split('.')[0]
        
        if tcp_id not in processed_ids:
        
            if len(LIMIT_TO_IDS) > 0:
                if tcp_id in LIMIT_TO_IDS:
                    q.put(p)
            else:
                q.put(p)

    for i in range(NUM_THREADS):
        
      worker = Thread(target=check_em_all, args=(q, i, TCP_FOLDER, AM_FOLDER, RESULTS_FOLDER))
      worker.setDaemon(True)
      worker.start()
        
    q.join()

    print('Done!')
    
    subprocess.getoutput('rm temp*')
