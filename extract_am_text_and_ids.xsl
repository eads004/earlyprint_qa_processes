<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
    version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    
    <xsl:output 
        method="text" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="no"/>
        
    <xsl:template match="tei:teiHeader"/>
    <xsl:template match="tei:label[@type='milestone']"/>
    <xsl:template match="tei:figDesc"/>
    <xsl:template match="tei:ref"/>
 
    <xsl:template match="tei:w">
        <xsl:variable name="content">
            <xsl:if test="@orig">
                <xsl:value-of select="@orig"/>
            </xsl:if>
            <xsl:if test="not(@orig)">
                <xsl:value-of select="."/>
            </xsl:if>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="@n='DELETE'"/> 
            <xsl:when test="@xml:id"> 
                <xsl:value-of select="replace($content, '•', '●')"/><xsl:text>&#x9;</xsl:text><xsl:value-of select="@xml:id"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="replace($content, '•', '●')"/><xsl:text>&#x9;id-missing</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:pc">
        <xsl:choose>
            <xsl:when test="@n='DELETE'"/> 
            <xsl:when test="@xml:id"> 
                <xsl:value-of select="replace(text(), '•', '●')"/><xsl:text>&#x9;</xsl:text><xsl:value-of select="@xml:id"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="replace(text(), '•', '●')"/><xsl:text>&#x9;id-missing</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:sic">
        <xsl:choose>
            <xsl:when test="@n='DELETE'"/> 
            <xsl:when test="@xml:id"> 
                <xsl:value-of select="replace(text(), '•', '●')"/><xsl:text>&#x9;</xsl:text><xsl:value-of select="@xml:id"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="replace(text(), '•', '●')"/><xsl:text>&#x9;id-missing</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
